import csv, os
"""
    Fonction verif_csv
    %       Cette fonction vérifie que le fichier existe que ce dernier
            est bien un fichier et qu'il se termine bien par '.csv'.
    %IN     path : Chaine de caractère représentant le chemin à un fichier
    %OUT    result : Booléen indiquant si le fichier est conforme ou non
"""
def verif_csv(path):
    if os.path.exists(path) and os.path.isfile(path) and path.split('.')[len(path.split('.')) - 1] == "csv":
        return True
    else:
        return False