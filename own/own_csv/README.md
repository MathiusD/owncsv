# Module own_csv

## Description

Ce module nous permet d'extraire et de vérifier qu'un fichier csv est conforme.

### Fonctions

Dans ce module on possède que 2 fonctions :

* read_csv() qui extrait les données d'un fichier csv.
* verif_csv() qui nous indique si un fichier est un bien un fichier csv valide.

### Tests

Les tests sont dans le module tests du projet au sein du fichier test_csv. On y teste divers path pour verif_csv et teste l'extraction de read_csv sur un fichier csv présent dans le module de test.

### Dépendances

Ce module dépend du module constructeur csv, logging et os.
