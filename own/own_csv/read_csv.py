import csv
import logging

"""
    Fonction read_csv
    %       Cette fonction lit un fichier csv passé en argument à l'aide du
            délimiteur passé en argument.
            Elle créé alors une liste de dictionnaire donc le premier est
            un pattern correspondant aux diverses catégories du fichier
            avant de compléter les autres dictionnaires selon ce pattern
            avec les données récoltées.
    %IN     path : Chaine de caractère représentant le chemin à un fichier,
            delimiteur : Caractère délimiteur au sein du fichier
    %OUT    list_data : Liste de dictionnaire correspondant au fichier csv
"""
def read_csv(path, delimiteur):
    list_data = []
    names = []
    logging.debug("Construction of Dictionnary")
    with open(path) as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiteur)
        i = 0
        for row in reader:
            if len(names) == 0:
                logging.debug("Extract Names of Column")
                for name in row:
                    names.append(name)
            else:
                list_data.append({})
                for indice in range(len(row)):
                    list_data[i][names[indice]] = row[indice]
                i = i + 1
    logging.debug("Data Found : " + str(i))
    return list_data