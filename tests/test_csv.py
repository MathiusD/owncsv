import unittest
from own.own_csv.read_csv import read_csv
from own.own_csv.verif_csv import verif_csv

class Test_Own_CSV(unittest.TestCase):

    def test_read_csv(self):
        reponse_1 = [
            {'example_1': "abc_3", "example_2": "156_9", "example_3": "456_4"},
            {'example_1': "101010_ui", "example_2": "oui_non", "example_3": "~_1"}
        ]
        reponse_2 = [
            {'example': 'abc', "1/example": "3/156", "2/example": "9/456", "3":'4'},
            {'example': '101010', "1/example": "ui/oui", "2/example": "non/~", "3":'1'}
        ]
        delimiteur_1 = "/"
        delimiteur_2 = "_"
        path = "tests/test.csv"
        self.assertEqual(reponse_1, read_csv(path, delimiteur_1))
        self.assertEqual(reponse_2, read_csv(path, delimiteur_2))
    
    def test_verif_csv(self):
        self.assertEqual(False, verif_csv("tests/test_csv.py"))
        self.assertEqual(True, verif_csv("tests/test.csv"))